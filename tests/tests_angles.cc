#include "gtest/gtest_pred_impl.h" // for AssertionResult, ASSERT_FALSE
#include <gtest/gtest-message.h>   // for Message
#include <gtest/gtest-test-part.h> // for TestPartResult

#include "Tifini3Config.h"

#include "KTools.h"

#include <TLorentzVector.h>

#if defined(HYDRA1COMP) && !defined(HYDRA1COMPBT)
#include "KTools.h"
#else
#include "hparticletool.h"
#endif

TEST(tests_angles, angle_ranges)
{
    TLorentzVector lvec;

    // In ROOT, Phi goes -pi/2 to pi/2 (radians)
    lvec.SetPxPyPzE(1, 0, 1, 1);
    ASSERT_FLOAT_EQ(lvec.Phi(), 0);

    lvec.SetPxPyPzE(0, 1, 0, 1);
    ASSERT_FLOAT_EQ(lvec.Phi(), M_PI_2);

    lvec.SetPxPyPzE(-1, 0, 1, 1);
    ASSERT_FLOAT_EQ(lvec.Phi(), M_PI);

    lvec.SetPxPyPzE(0, -1, 0, 1);
    ASSERT_FLOAT_EQ(lvec.Phi(), -M_PI_2);

    KVirtualCand kvcand;
    // In Hydra, Theta and Phi goes -90, 90
    kvcand.setMomentum(100);
    kvcand.setTheta(90);
    kvcand.setPhi(0);
    kvcand.calc4vectorProperties();

    ASSERT_FLOAT_EQ(kvcand.Phi(), 0);
    ASSERT_FLOAT_EQ(kvcand.getPhi(), 0);

    kvcand.setPhi(90);
    kvcand.calc4vectorProperties();

    ASSERT_FLOAT_EQ(kvcand.Phi(), M_PI_2);
    ASSERT_FLOAT_EQ(kvcand.getPhi(), 90);

    kvcand.setPhi(180);
    kvcand.calc4vectorProperties();

    ASSERT_FLOAT_EQ(kvcand.Phi(), M_PI);
    ASSERT_FLOAT_EQ(kvcand.getPhi(), 180);

    kvcand.setPhi(270);
    kvcand.calc4vectorProperties();

//     ASSERT_FLOAT_EQ(kvcand.Phi(), -M_PI_2); FIXME Why not working?
    ASSERT_FLOAT_EQ(kvcand.getPhi(), 270);


    const int n = 120;
    for (int i = 0; i < n; ++i) {
        float angle = 360./n * i;
        float px = cos(2*M_PI/n * i);
        float py = sin(2*M_PI/n * i);
        lvec.SetPxPyPzE(px, py, 0, 1);
//         printf("Angle = %3.4f  ", angle);
//         lvec.Print();

        kvcand.setPhi(angle);
        kvcand.calc4vectorProperties();

        ASSERT_FLOAT_EQ(HParticleTool::getLabPhiDeg(lvec), angle);
        ASSERT_FLOAT_EQ(HParticleTool::getLabPhiDeg(lvec), kvcand.getPhi());
    }

    // conversions

//     HParticleTool::getLabPhiDeg(*c2)
}
