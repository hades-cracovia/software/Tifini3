/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Rafal Lalik <rafal.lalik@ph.tum.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef KTIFINIRUNNER_H
#define KTIFINIRUNNER_H

#include "Tifini3Config.h"
#include "KTifiniTypes.h"
#include "KTools.h"

#ifdef HYDRA1COMP
#include <hrootsource.h>
#include <TChain.h>
class Hades;
#else
#include <hloop.h>
#endif

const Int_t N_MDC_momdedx_spec = 7;
const Int_t N_TOF_momdedx_spec = 7;

#include <ugly_getopt.h>

#include <TCanvas.h>
#include <TCutG.h>
#include <TChain.h>
#include <TH1.h>
#include <TH2.h>
#include <TF1.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TNtuple.h>
#include <Rtypes.h>
#include <TApplication.h>

class KTifiniAnalysis;

class KTifiniRunner
{
public:
    KTifiniRunner(int argc, char ** argv, KT::AnalysisType at, KTifiniAnalysis * ana);
    virtual ~KTifiniRunner();

    void exec();

    inline Int_t getEventsNumber() { return eventsnr; }

protected:
    bool config_handler(int code, const char* optarg);
    // int Configure(int argc, char ** argv);
    void Usage() const;

private:
    void exportCanvas();


private:
    KT::AnalysisType analysisType;
    KT::Experiment expName;

    // general:
#ifdef HYDRA1COMP
    Hades* myHades;
    HRootSource* source;
    TTree * loop;
#else
    HLoop * loop;                  // standard input
#endif

    KTifiniAnalysis * ana;

    // Configuration flags
    ugly_getopt ugly;
    int argc_; char ** argv_;
    int flag_verbose;
    int flag_list;
    int flag_nopbar;
    long eventstart;
    long eventsnr;
    TString filedir;
    TString file_out;
    TString outdir;
    TString suffix;
};

#endif // KTIFINIRUNNER_H
