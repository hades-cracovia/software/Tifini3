/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011 Rafał Lalik <rafal.lalik@ph.tum.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef KTIFINITYPES_H
#define KTIFINITYPES_H

namespace KT
{
    // Cut type
    enum CutType
    {
        DEDX, BETA
    };

    //
    enum DetSystem
    {
        NOSYSTEM = 0x00,
        MDC = 0x01,
        TOF = 0x02,
        TOFINO = 0x04,
        RPC = 0x08,
        STRAW = 0x10,
        FDRPC = 0x20
    };

    // particle cut ID
    enum CutID
    {
        cut_p = 0, cut_pip, cut_pim, cut_Kp, cut_Km, cut_el, cut_ep, CID_Dummy
    };

    //
    enum AnalysisType { Exp = 0, Sim = 1 };

    //
    enum Experiment { exp_none, apr07, sep08, pp45 };

    // Particle ID (compatible with Geant ID)
    enum ParticleID
    {
        PID_None = 0,   ///< dont use, no geant PID == 0
        gamma = 1,      ///< gamma
        elp = 2,        ///< positron
        elm = 3,        ///< electron
        mup = 5,        ///< positive muon
        mum = 6,        ///< negative muon
        pip = 8,        ///< positive pion
        pim = 9,        ///< negative pion
        Kp = 11,        ///< positive Kaon
        Km = 12,        ///< negative Kaon
        p = 14,         ///< proton
        PID_Dummy       ///< don't use it, it's for limits calculation
    };

    //
    enum KPidSelectionType
    {
        Charge = 0x01,          ///< select PID by particle charge
        Beta = 0x02,            ///< select PID by particle Beta
        Graphical_dEdx = 0x04,  ///< select PID by graphical cuts
        PST_Dummy               ///< don't use it, it's for limits calculation
    };

    enum TriggerSelection
    {
        LVL1,
        LVL2
    };

    enum DownscalingSelection
    {
        AllEvents,
        OnlyDownscaled,
        NoDownscaled
    };
}

#endif // KTIFINITYPES_H
