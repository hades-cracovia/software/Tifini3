/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Rafał Lalik <rafal.lalik@ph.tum.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "KTifiniRunner.h"

#ifdef HAS_HADESDEDXEQUALIZER
#include <hades_dEdx_reader.h>
#endif

#include "KPlotsBeta.h"
#include "KPlotsdEdx.h"

#if defined(HYDRA1COMP)
#include <heventheader.h>
#include <hpidevtinfo.h>
#include <hpidtrackcandsim.h>
#include <hpidtracksorter.h>
#include <TChain.h>
#ifdef HYDRA1COMPBT
#include <hcategorymanager.h>
#endif
#else
#include <forwarddef.h>
#include <hcategorymanager.h>
#include <hparticletracksorter.h>
#endif

#include <getopt.h>

#ifdef HYDRA1COMP
TTree* gTree = nullptr;
#endif

#if defined(HYDRA1COMP)
static Bool_t selectHadronsQa(HPidTrackCand* pcand)
{
    Bool_t test = kFALSE;
    if (pcand->isFlagAND(4, HPidTrackCand::kIsAcceptedHitInnerMDC,
                         HPidTrackCand::kIsAcceptedHitOuterMDC, HPidTrackCand::kIsAcceptedHitMETA,
                         HPidTrackCand::kIsAcceptedRK) &&
        pcand->getTrackData()->getRKChiSquare() < 1000000)
        test = kTRUE;

    return test;
}

static Bool_t selectHadronsQa_NoMETA(HPidTrackCand* pcand)
{
    Bool_t test = kFALSE;
    if (pcand->isFlagAND(3, HPidTrackCand::kIsAcceptedHitInnerMDC,
                         HPidTrackCand::kIsAcceptedHitOuterMDC,
                         //            HPidTrackCand::kIsAcceptedHitMETA,
                         HPidTrackCand::kIsAcceptedRK) &&
        pcand->getTrackData()->getRKChiSquare() < 1000000)
        test = kTRUE;

    return test;
}

#else
static Bool_t selectHadronsQa(HParticleCand* pcand)
{
    Bool_t test = kFALSE;
    if (pcand->isFlagAND(4, // Particle::kIsAcceptedHitRICH,
                         Particle::kIsAcceptedHitInnerMDC, Particle::kIsAcceptedHitOuterMDC,
                         Particle::kIsAcceptedHitMETA, Particle::kIsAcceptedRK) &&
        //        pcand->isFlagBit(kIsUsed)
        //        &&
        pcand->getInnerSegmentChi2() > 0 && // pcand->getBeta>0.9 && pcand->getBeta<1.1
        pcand->getChi2() < 1000             // RK       //MetaQA<4
    )
        test = kTRUE;

    return test;
}
#endif

KTifiniRunner::KTifiniRunner(int argc, char** argv, KT::AnalysisType at, KTifiniAnalysis* ana)
    : analysisType(at), expName(KT::exp_none), ana(ana), flag_verbose(0), flag_list(0),
      flag_nopbar(0), eventstart(0), eventsnr(-1)
{
#ifdef HYDRA1COMP
    myHades = new Hades;
    gHades->setQuietMode(2);
    source = new HRootSource();
#else
    loop = new HLoop(kTRUE);
#endif

    /*int optsleft = */
    // Configure(argc, argv);

    ana->setVerboseFlag(flag_verbose);

    auto h = std::bind(&KTifiniRunner::config_handler, this, std::placeholders::_1, std::placeholders::_2);

    ugly.add_option("verbose", no_argument, &flag_verbose, 1).set_option_description("Make verbose").set_handler(h);
    ugly.add_option("brief", no_argument, &flag_verbose, 0).set_option_description("Make brief").set_handler(h);
    ugly.add_option("no-progress-bar", no_argument, &flag_nopbar, 1).set_option_description("no progress bar").set_handler(h);
    /* These options don't set a flag.
     We disting*uish them by their indices. */
    ugly.add_option("help", no_argument, 0, 'h').set_option_description("display help (this)").set_handler(h);
    ugly.add_option("events", required_argument, 0, 'e').set_option_description("number of events").set_handler(h);
    ugly.add_option("start", required_argument, 0, 't').set_option_description("start event").set_handler(h);
    ugly.add_option("dir", required_argument, 0, 'd').set_option_description("output directory").set_handler(h);
    ugly.add_option("output", required_argument, 0, 'o').set_option_description("output file").set_handler(h);
    ugly.add_option("suffix", required_argument, 0, 's').set_option_description("output file suffix").set_handler(h);

    ana->configureCmdl(ugly);
    ugly.usage(argc, argv);
    ugly.configure(argc, argv);

    if (optind < argc)
    {
        Bool_t ret = kFALSE;
        while (optind < argc)
        {
            TString infile = argv[optind++];
            if (flag_verbose) {
                std::cout << "-- Adding file " << infile.Data() << std::endl;
            }
#ifdef HYDRA1COMP
            if (infile.Contains(".root"))
            {
                ret = source->addFile(infile);
                if (file_out.Length() == 0) file_out = infile;
            }
#else
            if (infile.Contains(","))
                ret = loop->addMultFiles(infile);
            else if (infile.Contains(".root"))
            {
                ret = loop->addFiles(infile);
                if (file_out.Length() == 0) file_out = infile;
            }
            else
            {
                ret = loop->addFilesList(infile);
                if (file_out.Length() == 0) file_out = infile;
            }
#endif
            if (file_out.Length() == 0) file_out = "output.root";

            if (!ret)
            {
                std::cerr << "READBACK: ERROR : cannot find inputfiles : " << infile.Data() << endl;
                std::exit(EXIT_FAILURE);
            }
        }
    }

    // return optind;
}

KTifiniRunner::~KTifiniRunner()
{
    std::cout << "++= Tifnini finished it's job! +++" << std::endl;
}

void KTifiniRunner::exportCanvas() { ana->getCuts().drawCuts(); }

void KTifiniRunner::exec()
{
    if (!ana)
    {
        std::cerr << "No analysis registered! Exiting..." << std::endl;
        std::exit(EXIT_FAILURE);
    }

    expName = ana->getExperimentType();

    if (expName == KT::exp_none)
    {
        std::cerr << "No analysis/beamtime selected! Exiting..." << std::endl;
        std::exit(EXIT_FAILURE);
    }

    std::cout << "******************************" << std::endl;
    if (analysisType == KT::Exp)
        std::cout << "Parameters for experiment data are loaded" << std::endl;
    if (analysisType == KT::Sim)
        std::cout << "Parameters for simulated data are loaded" << std::endl;
    std::cout << "******************************" << std::endl;

    ana->initAnalysis(expName, analysisType);

    gStyle->SetPalette(1);
    gStyle->SetCanvasColor(10);

    TString outputfile_suffix; // suffix for the output file

    if (analysisType == KT::Exp) outputfile_suffix = "_exp_";
    if (analysisType == KT::Sim) outputfile_suffix = "_sim_";

    outputfile_suffix += "_" + suffix;
    if (!suffix.IsNull()) outputfile_suffix += "_" + suffix;

    outputfile_suffix += ".root";

    if (!filedir.IsNull() and !filedir.EndsWith("/")) filedir = filedir + "/";

    TString filename_in = file_out;

    if (filename_in.EndsWith(".txt", TString::kIgnoreCase))
    {
        filename_in.Remove(file_out.Length() - 4, 4);
    }
    else if (filename_in.EndsWith(".lst", TString::kIgnoreCase))
    {
        filename_in.Remove(file_out.Length() - 4, 4);
    }
    else if (filename_in.EndsWith(".root", TString::kIgnoreCase))
    {
        filename_in.Remove(file_out.Length() - 5, 5);
    }

    if (!filename_in.BeginsWith("/")) filename_in = filedir + filename_in;

    file_out = filename_in + outputfile_suffix;

    if (!outdir.IsNull())
    {
        if (!outdir.EndsWith("/")) outdir = outdir + "/";

        Ssiz_t s = file_out.Last('/');
        if (s == -1)
            file_out = outdir + file_out;
        else { file_out = outdir + file_out(s, 1000); }
    }

    // OUTPUT file is created
    std::cout << "Output file: " << file_out << std::endl;
    // - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

#ifdef HAS_HADESDEDXEQUALIZER
    HadesdEdxReader dEdx_data("bindata/apr07_dEdx_fits.dat", "bindata/apr07_dEdx_fits_TOF.dat");
    dEdx_per_run dEdx_run_data_ref = dEdx_data.get_run_data(1568058751);
    dEdx_per_run dEdx_run_data;
    Long_t current_run_id = 0;
#endif

#ifdef HYDRA1COMP
    myHades->setDataSource(source);

    if (!myHades->init())
    {
        cout << "Hades Init() failed!" << endl;
        exit(1);
    }

    loop = source->getTree();
    Long64_t num_events = loop->GetEntries();
#else
    if (!loop->setInput(""))
    {
        std::cerr << "READBACK: ERROR : cannot read input !" << std::endl;
        std::exit(EXIT_FAILURE);
    }
    Long64_t num_events = loop->getEntries();
#endif
    Long64_t first_event = eventstart;

    if (first_event < 0) first_event = 0;

    Long64_t nEvents = eventsnr;
    if ((nEvents == -1) or (nEvents > num_events)) nEvents = num_events;
    std::cout << "Number of events to analyze: " << nEvents << std::endl;

    // get hydra categories
    HEvent* fEvent = gHades->getCurrentEvent();
    if (!fEvent) exit(1);

#ifdef HYDRA1COMP
    HCategory* fCatCand = (HCategory*)gHades->getCurrentEvent()->getCategory(catPidTrackCand);
#ifdef HYDRA1COMPBT
    HCategory* fCatCand_BT = (HCategory*)gHades->getCurrentEvent()->getCategory(catParticleCand);
#endif
#else
    HCategory* fCatCand =
        (HCategory*)HCategoryManager::getCategory(catParticleCand, 1, "HParticleCandSim");
    HCategory* fForwardCand =
        (HCategory*)HCategoryManager::getCategory(catForwardCand, 1, "catForwardCand");
    HCategory* catHParticleEvtInfo =
        (HCategory*)HCategoryManager::getCategory(catParticleEvtInfo, kTRUE, "catParticleEvtInfo");
    /*HIterator* iterParticleEvtInfo =*/(HIterator*)catHParticleEvtInfo->MakeIterator("native");

    if (!fForwardCand) exit(1);
#endif

    if (!fCatCand)
    {
        std::cerr << "ERROR: No Cand category" << std::endl;
        exit(1);
    }

    // prepare output tree
    TFile* Outputfile = new TFile(file_out, "RECREATE");
    Outputfile->cd();

    std::cout << "******* Start of event loop ********" << std::endl;
    std::cout << "" << std::endl;

    ana->preAnalysis();

    KPlotsdEdx dEdx_plots("global_dEdx");
    KPlotsBeta beta_plots("global_beta");

    switch (expName)
    {
        case KT::apr07:
        case KT::sep08:
            //    dEdx_plots.init(KT::MDC | KT::TOF | KT::TOFINO, "charge * momentum / q * MeV/c",
            //    "dEdx / [a.u.]",
            //                    KPlotsdEdx::mom_bins, KPlotsdEdx::mom_min, KPlotsdEdx::mom_max,
            //                    KPlotsdEdx::dEdx_bins, KPlotsdEdx::dEdx_min,
            //                    KPlotsdEdx::dEdx_max);
            dEdx_plots.init(KT::MDC, "charge * momentum / q * MeV/c", "dEdx / [a.u.]", 500, -1500,
                            2500, 500, 0, 10, false);
            dEdx_plots.init(KT::TOFINO, "charge * momentum / q * MeV/c", "dEdx / [a.u.]", 500,
                            -1200, 2000, 500, 0, 4, false);
            dEdx_plots.init(KT::TOF, "charge * momentum / q * MeV/c", "dEdx / [a.u.]", 500, -1200,
                            2000, 500, 0, 8, false);

            beta_plots.init(KT::TOF | KT::TOFINO, "charge * momentum / q * MeV/c", "beta / c",
                            KPlotsBeta::mom_bins, KPlotsBeta::mom_min, KPlotsBeta::mom_max,
                            KPlotsBeta::beta_bins, KPlotsBeta::beta_min, KPlotsBeta::beta_max);
            break;
        case KT::pp45:
            dEdx_plots.init(KT::MDC, "charge * momentum / q * MeV/c", "dEdx / [a.u.]", 500, -1500,
                            2500, 500, 0, 10, false);
            dEdx_plots.init(KT::TOF, "charge * momentum / q * MeV/c", "dEdx / [a.u.]", 500, -1200,
                            2000, 500, 0, 8, false);

            beta_plots.init(KT::RPC | KT::TOF, "charge * momentum / q * MeV/c", "beta / c",
                            KPlotsBeta::mom_bins, KPlotsBeta::mom_min, KPlotsBeta::mom_max,
                            KPlotsBeta::beta_bins, KPlotsBeta::beta_min, KPlotsBeta::beta_max);
            break;
        default:
            break;
    }

#ifdef HYDRA1COMP
    KT::DownscalingSelection ds_selection = ana->getDownscalingSelector();

    HPidTrackSorter sorter;
#else
    HParticleTrackSorter sorter;
#endif
    sorter.init();

    bool g_is_valid[100000] = {false};

    constexpr int dot_size = 500;
    constexpr int bar_size = 10000;

    // loop over all events
    Int_t event_num = first_event;
    for (; event_num < nEvents; event_num++)
    {
#ifdef HYDRA1COMP
        if (!gHades->eventLoop(1, 0))
        {
            if (!flag_nopbar) cout << " end reached " << endl;
            break;
        }
#else
        if (loop->nextEvent(event_num) <= 0)
        {
            if (!flag_nopbar) cout << " end reached " << endl;
            break;
        }
#endif

        if (!flag_nopbar)
        {
            if (event_num != 0 && event_num % dot_size == 0) std::cout << "." << std::flush;

            if (event_num == 0) std::cout << std::flush << "==> Processing data " << std::flush;

            if (event_num != 0 && event_num % bar_size == 0)
            {
                Float_t event_percent = 100.0 * event_num / nEvents;
                std::cout << " " << event_num << " (" << event_percent << "%) " << std::endl
                          << "==> Processing data " << std::flush;
            }
        }

        // downscaling
#ifdef HYDRA1COMP
        UInt_t downscaling_flag = fEvent->getHeader()->getDownscalingFlag();
        if (analysisType == KT::Exp and !(ds_selection == KT::AllEvents) and
            !((ds_selection == KT::OnlyDownscaled) and (downscaling_flag)) and
            !((ds_selection == KT::NoDownscaled) and (!downscaling_flag)))
            continue;
#endif

        ana->resetEvent();

#ifdef HYDRA1COMP
        Long_t run_id = source->getCurrentRunId();
#else
        Long_t run_id = 0; // FIXME
#endif

#ifdef HAS_HADESDEDXEQUALIZER
        if (run_id != current_run_id)
        {
            current_run_id = run_id;
            dEdx_run_data = dEdx_data.get_run_data(run_id);
        }
#endif

#ifdef HYDRA1COMP
        // FIXME
#else
        HParticleEvtInfo* evtinfo = HCategoryManager::getObject(evtinfo, catParticleEvtInfo, 0);
        if (!ana->setEventInfo(evtinfo)) { continue; }

        /*Int_t RpcMult       =*/evtinfo->getSumRpcMultHitCut();
        /*Int_t TofMult       =*/evtinfo->getSumTofMultCut();
        /*Int_t SelectedPart  =*/evtinfo->getSumSelectedParticleCandMult();
#endif

        sorter.cleanUp();
        sorter.resetFlags(kTRUE, kTRUE, kTRUE,
                          kTRUE); // reset all flags for flags (0-28), reject, used, lepton
        /*Int_t nCandHad = */ sorter.fill(
            selectHadronsQa); // fill only good hadrons (already marked good leptons will be skipp)
        // Int_t nCandHad     = sorter.fill(selectHadronsQa_noMeta2);   // fill only good hadrons
        // (already marked good leptons will be skipp$
//         Int_t nCandHad     = sorter.fill(HParticleTrackSorter::selectHadrons);   // fill only
//         good hadrons (already marked good leptons will be skipp$
#ifdef HYDRA1COMP
        /*Int_t nCandHadBest = */ sorter.selectBest(HPidTrackSorter::kIsBestRKRKMETA,
                                                    HPidTrackSorter::kIsHadron);

        /*Int_t nCandHad_NoMETA = */ sorter.fill(
            selectHadronsQa_NoMETA); // fill only good hadrons (already marked good leptons will be
                                     // skipp)
        /*Int_t nCandHadBest_NoMETA = */ sorter.selectBest(HPidTrackSorter::kIsBestRK,
                                                           HPidTrackSorter::kIsHadron);

#else
        /*Int_t nCandHadBest =*/sorter.selectBest(Particle::kIsBestRKSorter,
                                                  Particle::kIsHadronSorter);
#endif
        Int_t cand_size = fCatCand->getEntries();
#ifndef HYDRA1COMP
        Int_t vect_size = fForwardCand->getEntries(); // number of Emc hits in this event
#endif
        //#### HADES Tracks ####
        // loop over all tracks of the actual event

        for (Int_t i = 0; i < cand_size; ++i)
        {
#if defined(HYDRA1COMP) // && !defined(HYDRA1COMPBT)
            HPidTrackCand* tcand = (HPidTrackCand*)fCatCand->getObject(i);

            g_is_valid[i] = tcand->isFlagBit(HPidTrackCand::kIsUsed);
            if (!g_is_valid[i]) continue;

#ifdef HAS_HADESDEDXEQUALIZER
            if (dEdx_run_data.runid != 0)
            {
                HPidHitData* hd = tcand->getHitData();
                Int_t sector = hd->getSector();
                Float_t mdc_dEdx = hd->getCombinedMdcdEdx();
                Float_t mdc_dEdx_c = correct_dEdx(
                    mdc_dEdx, dEdx_run_data.mdc_mean[sector], dEdx_run_data.mdc_sigma[sector],
                    dEdx_run_data_ref.mdc_mean[3], dEdx_run_data_ref.mdc_sigma[3]);
                hd->setCombinedMdcdEdx(mdc_dEdx_c);

                Int_t system = hd->getSystem();
                Int_t cell = hd->getMetaCell();
                if (0 == system)
                {
                    HPidTrackData* td = tcand->getTrackData();
                    Float_t tofino_dEdx = td->fCorrectedEloss[4];
                    Float_t tofino_dEdx_c = correct_dEdx(
                        tofino_dEdx, dEdx_run_data.tofino_mean[sector][cell],
                        dEdx_run_data.tofino_sigma[sector][cell],
                        dEdx_run_data_ref.tofino_mean[0][3], dEdx_run_data_ref.tofino_sigma[0][3]);
                    td->fCorrectedEloss[4] = tofino_dEdx_c;
                }
                else if (1 == system)
                {
                    HPidTrackData* td = tcand->getTrackData();
                    Float_t tof_dEdx = td->fCorrectedEloss[4];
                    Float_t tof_dEdx_c = correct_dEdx(
                        tof_dEdx, dEdx_data.get_Tof_dEdx_mean(sector, cell),
                        dEdx_data.get_Tof_dEdx_sigma(sector, cell),
                        dEdx_data.get_Tof_dEdx_mean(0, 20), dEdx_data.get_Tof_dEdx_sigma(0, 20));
                    td->fCorrectedEloss[4] = tof_dEdx_c;
                }
            }
#endif

            KParticleCand* track = nullptr;
            if (analysisType == KT::Sim)
            {
                KParticleCandSim* _track = new KParticleCandSim;
                *_track = *(HPidTrackCandSim*)tcand;
                track = _track;
            }
            else
            {
                track = new KParticleCand;
                *track = *tcand;
            }
#else
            HParticleCand* track = HCategoryManager::getObject(track, fCatCand, i);
            g_is_valid[i] = track->isFlagBit(kIsUsed);
            if (!g_is_valid[i]) continue;
#endif
            ana->setHadesTrackInfo(track, i);
            dEdx_plots.fill(track);
            beta_plots.fill(track);
        }

#ifndef HYDRA1COMP
        for (Int_t i = 0; i < vect_size; ++i)
        {
            HForwardCand* track = HCategoryManager::getObject(track, fForwardCand, i);
            // if (!tcand->isFlagBit(HPidTrackCand::kIsUsed)) continue;
            ana->setForwardTrackInfo(track, i);
            //             dEdx_plots.fill(track);  FIXME
            //             beta_plots.fill(track);
        }
#endif

        //#### ANALYSIS ####
        ana->analysis(fEvent, event_num, run_id); // TODO replace 0 with run_id

        size_t ti_size = ana->getNHadesTrackInfo();
        for (UInt_t i = 0; i < ti_size; ++i)
        {
            TrackInfo ti = ana->getHadesTrackInfo(i);
            if (ti.is_used)
            {
#if defined(HYDRA1COMP) // && !defined(HYDRA1COMPBT)
                HPidTrackCand* tcand = (HPidTrackCand*)fCatCand->getObject(ti.tid);

                KParticleCand* track = nullptr;
                if (analysisType == KT::Sim)
                {
                    KParticleCandSim* _track = new KParticleCandSim;
                    *_track = *(HPidTrackCandSim*)tcand;
                    track = _track;
                }
                else
                {
                    track = new KParticleCand;
                    *track = *tcand;
                }
#else
                HParticleCand* track = HCategoryManager::getObject(track, fCatCand, ti.tid);
#endif
                dEdx_plots.fill_acc(track);
                beta_plots.fill_acc(track);
            }
        }
    } // End event loop

    std::cout << nEvents - first_event << " events analyzed" << std::endl;

    ana->postAnalysis();

    Outputfile->cd();

    dEdx_plots.drawPlots(gDirectory);
    beta_plots.drawPlots(gDirectory);

    ana->finalizeAnalysis();

    exportCanvas();

    std::cout << "******* End of program ********" << std::endl;
}

bool KTifiniRunner::config_handler(int code, const char* optarg)
{
    switch (code)
    {
        case 't':
            eventstart = atoi(optarg);
            break;

        case 'e':
            eventsnr = atoi(optarg);
            break;

        case 'o':
            file_out = optarg;
            break;

        case 'd':
            outdir = optarg;
            break;

        case 's':
            suffix = optarg;
            break;

        case 'h':
            Usage();
            exit(EXIT_SUCCESS);

        // case '?':
        //     /* getopt_long already printed an error message. */
        //     ana->Configure(c, optarg); FIXME
        //     break;

        default:
            return false;
    }

    return true;
}

void KTifiniRunner::Usage() const
{
    std::cout << "options: \n"
              << " -e | --events [value]\t\t - number of events to proceed, default is 100000\n"
              << " -d | --filedir [path]\t\t - path to directory with input file, default is "
                 "current directory\n"
              << " -f | --file [filename]\t\t - filename, if relative filename then path from '-d' "
                 "is used\n"
              << " -k | --listdir [path]\t\t - path to directory with input list, default is "
                 "current directory\n"
              << " -l | --file [filename]\t\t - listname, if relative listname then path from '-k' "
                 "is used\n"
              << " -o | --outdir [string]\t\t - directory for output files, default is empty\n"
              << " -s | --suffix [string]\t\t - optional suffix for output files, default is "
                 "username from $USER\n"
              << "      --with-wall\t\t - add ForwardWall for PID to analysis\n"
              << "      --verbose\t\t\t - speak more\n"
              << "      --brief\t\t\t - speak less [default]\n"
              << "\n" << std::endl;

    if (ana) { ana->Usage(); }

    exit(EXIT_SUCCESS);
}
