/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011  Rafał Lalik <rafal.lalik@ph.tum.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <cstring>

#include <getopt.h>

#include <TTree.h>

#include <hphysicsconstants.h>

#include "KTifiniAnalysis.h"
#include "KCutInside.h"

TrackInfo::TrackInfo()
{
    for(Int_t ipid = 0; ipid < KT::PID_Dummy; ++ipid)
        for(Int_t ipst = 0; ipst < KT::PST_Dummy; ++ipst)
            pid[ipid][ipst] = false;
    cand = nullptr;
}

KTifiniAnalysis::KTifiniAnalysis(const TString & analysisName, const TString & treeName)
    : expType(KT::exp_none)
    , _analysisName(analysisName)
    , verbose_flag(false)
{
    tree = new TTree(treeName, analysisName);

    // event info
    tree->Branch("eGoodEvent",          &eGoodEvent,        "eGoodEvent/O");
    tree->Branch("eGoodTrigger",        &eGoodTrigger,      "eGoodTrigger/O");
    tree->Branch("eGoodStart",          &eGoodStart,        "eGoodStart/O");
    tree->Branch("eGoodVertCluster",    &eGoodVertCluster,  "eGoodVertCluster/O");
    tree->Branch("eGoodVertCand",       &eGoodVertCand,     "eGoodVertCand/O");
    tree->Branch("eNoPileUpStart",      &eNoPileUpStart,    "eNoPileUpStart/O");

#ifndef HYDRA1COMP
    setGoodEventSelector(Particle::kGoodTRIGGER |
        Particle::kGoodVertexClust |
        Particle::kGoodVertexCand |
        Particle::kGoodSTART |
        Particle::kNoPileUpSTART);

    setDownscalingSelector(KT::AllEvents);
#endif

    setPidSelectionHades(KT::p, KT::Charge);
    setPidSelectionHades(KT::pim, KT::Charge);
    setPidSelectionHades(KT::pip, KT::Charge);
    setPidSelectionHades(KT::Km, KT::Charge);
    setPidSelectionHades(KT::Kp, KT::Charge);

    setPidSelectionForward(KT::p, KT::Beta);
    setPidSelectionForward(KT::pim, KT::Beta);
    setPidSelectionForward(KT::pip, KT::Beta);
    setPidSelectionForward(KT::Km, KT::Beta);
    setPidSelectionForward(KT::Kp, KT::Beta);
}

KTifiniAnalysis::~KTifiniAnalysis()
{
    delete tree;
//     delete cuts;
    std::cout << "+++ Analysis destructed! +++" << std::endl;
}

void KTifiniAnalysis::initAnalysis(KT::Experiment exp, KT::AnalysisType analysisType)
{
    std::cout << "+++ Preapring analysis... " << std::endl;

    this->analysisType = analysisType;

    configureGraphicalCuts(trackInspector);
    configureTree(tree);

    if (!tree)
    {
        std::cerr << "Tree in not initilized! Aborting..." << std::endl;
        exit(EXIT_FAILURE);
    }

    switch (exp)
    {
        case KT::pp45:
            bg = {0.0, 0.0, 1.0};
            tg = {0.0, 0.0, -90.0, 80.0};
            break;
        default:
            bg = {0.0, 0.0, 0.0};
            tg = {0.0, 0.0, 0.0, 0.0};
            break;
    }
    std::cout << "* Create analysis " << _analysisName << std::endl;
}

void KTifiniAnalysis::finalizeAnalysis()
{
    std::cout << "+++ Finalizing Analysis +++" << std::endl;

    if (tree) {
        tree->Write();
        if (verbose_flag)
            tree->Print();
    }
}

void KTifiniAnalysis::resetEvent()
{
#ifdef HYDRA1COMP
    TrackInfoVec::iterator it;
    for (it = hades_tracks.begin(); it != hades_tracks.end(); ++it)
        delete it->cand;
#endif
    hades_tracks.clear();
#ifndef HYDRA1COMP
    forward_tracks.clear();
#endif
}

#ifndef HYDRA1COMP
Bool_t KTifiniAnalysis::setEventInfo(HParticleEvtInfo* evtinfo)
{
    eGoodTrigger = evtinfo->isGoodEvent(Particle::kGoodTRIGGER);
    eGoodStart = evtinfo->isGoodEvent(Particle::kGoodSTART);
    eGoodVertCluster = evtinfo->isGoodEvent(Particle::kGoodVertexClust);
    eGoodVertCand = evtinfo->isGoodEvent(Particle::kGoodVertexCand);
    eNoPileUpStart = evtinfo->isGoodEvent(Particle::kNoPileUpSTART);
    eGoodEvent = evtinfo->isGoodEvent(good_event_selector);

    return eGoodEvent;
}
#endif

Bool_t KTifiniAnalysis::isGoodEvent() const
{
#ifndef HYDRA1COMP
    return eGoodEvent;
#else
    return kTRUE;
#endif
}

void KTifiniAnalysis::setHadesTrackInfo(KParticleCand * track, Int_t track_num)
{
    TrackInfo ti;
    ti.tid = track_num;
    ti.polarity = track->getCharge();
    ti.cand = track;
    PID(track, ti);
    hades_tracks.push_back(ti);
}

#ifndef HYDRA1COMP
void KTifiniAnalysis::setForwardTrackInfo(HForwardCand * track, Int_t track_num)
{
    TrackInfo ti;
    ti.tid = track_num;
    ti.polarity = 0;
    ti.cand = track;
    PID(track, ti);
    forward_tracks.push_back(ti);
}

void KTifiniAnalysis::PID(HForwardCand * /*track*/, TrackInfo & tinfo)
{
    if (pid_forward[KT::p] & KT::Charge) tinfo.pid[KT::p][KT::Charge] = true;
    if (pid_forward[KT::p] & KT::Beta) tinfo.pid[KT::p][KT::Beta] = true;
    if (pid_forward[KT::p] & KT::Graphical_dEdx) tinfo.pid[KT::p][KT::Graphical_dEdx] = true;

    if (pid_forward[KT::pip] & KT::Charge) tinfo.pid[KT::pip][KT::Charge] = true;
    if (pid_forward[KT::pip] & KT::Beta) tinfo.pid[KT::pip][KT::Beta] = true;
    if (pid_forward[KT::pip] & KT::Graphical_dEdx) tinfo.pid[KT::pip][KT::Graphical_dEdx] = true;

    if (pid_forward[KT::pim] & KT::Charge) tinfo.pid[KT::pim][KT::Charge] = true;
    if (pid_forward[KT::pim] & KT::Beta) tinfo.pid[KT::pim][KT::Beta] = true;
    if (pid_forward[KT::pim] & KT::Graphical_dEdx) tinfo.pid[KT::pim][KT::Graphical_dEdx] = true;

    if (pid_forward[KT::Kp] & KT::Charge) tinfo.pid[KT::Kp][KT::Charge] = true;
    if (pid_forward[KT::Kp] & KT::Beta) tinfo.pid[KT::Kp][KT::Beta] = true;
    if (pid_forward[KT::Kp] & KT::Graphical_dEdx) tinfo.pid[KT::Kp][KT::Graphical_dEdx] = true;

    if (pid_forward[KT::Km] & KT::Charge) tinfo.pid[KT::Km][KT::Charge] = true;
    if (pid_forward[KT::Km] & KT::Beta) tinfo.pid[KT::Km][KT::Beta] = true;
    if (pid_forward[KT::Km] & KT::Graphical_dEdx) tinfo.pid[KT::Km][KT::Graphical_dEdx] = true;
}
#endif

void KTifiniAnalysis::PID(KParticleCand* track, TrackInfo& tinfo)
{
    // proton = 14, pi+ = 8, pi- = 9, K+ = 11, K- = 12, e+ = 2, e- = 3 (GEANT PID definitions)
    // trackPIDs[j] Array (global definition) j: Protons == 0, Kaons == 1, Pions == 2, Electrons == 3
    // If System == -1 there is no META hit! MetaQual is also negative

    Short_t charge = track->getCharge();
    KTrackInspector & trackInsp = getCuts();
    trackInsp.clearCache();

    if (pid_hades[KT::p] & KT::Charge) tinfo.pid[KT::p][KT::Charge] = (charge == 1);
    if (pid_hades[KT::p] & KT::Beta) tinfo.pid[KT::p][KT::Beta] = makePID_Beta(track, KT::p);
    if (pid_hades[KT::p] & KT::Graphical_dEdx) tinfo.pid[KT::p][KT::Graphical_dEdx] = trackInsp.isInside(KT::DEDX, KT::cut_p, track);

    if (pid_hades[KT::pip] & KT::Charge) tinfo.pid[KT::pip][KT::Charge] = (charge == 1);
    if (pid_hades[KT::pip] & KT::Beta) tinfo.pid[KT::pip][KT::Beta] = makePID_Beta(track, KT::pip);
    if (pid_hades[KT::pip] & KT::Graphical_dEdx) tinfo.pid[KT::pip][KT::Graphical_dEdx] = trackInsp.isInside(KT::DEDX, KT::cut_pip, track);

    if (pid_hades[KT::pim] & KT::Charge) tinfo.pid[KT::pim][KT::Charge] = (charge == -1);
    if (pid_hades[KT::pim] & KT::Beta) tinfo.pid[KT::pim][KT::Beta] = makePID_Beta(track, KT::pim);
    if (pid_hades[KT::pim] & KT::Graphical_dEdx) tinfo.pid[KT::pim][KT::Graphical_dEdx] = trackInsp.isInside(KT::DEDX, KT::cut_pim, track);

    if (pid_hades[KT::Kp] & KT::Charge) tinfo.pid[KT::Kp][KT::Charge] = (charge == 1);
    if (pid_hades[KT::Kp] & KT::Beta) tinfo.pid[KT::Kp][KT::Beta] = makePID_Beta(track, KT::Kp);
    if (pid_hades[KT::Kp] & KT::Graphical_dEdx) tinfo.pid[KT::Kp][KT::Graphical_dEdx] = trackInsp.isInside(KT::DEDX, KT::cut_Kp, track);

    if (pid_hades[KT::Km] & KT::Charge) tinfo.pid[KT::Km][KT::Charge] = (charge == -1);
    if (pid_hades[KT::Km] & KT::Beta) tinfo.pid[KT::Km][KT::Beta] = makePID_Beta(track, KT::Km);
    if (pid_hades[KT::Km] & KT::Graphical_dEdx) tinfo.pid[KT::Km][KT::Graphical_dEdx] = trackInsp.isInside(KT::DEDX, KT::cut_Km, track);
// printf("* charge = %d, %d, %d\n", charge, KT::p, tinfo.pid[KT::p][KT::Charge]);
// printf("* charge = %d, %d, %d\n", charge, KT::pim, tinfo.pid[KT::pim][KT::Charge]);
// printf("  charge = %d  pid=%d\n", track->getCharge(), ((KParticleCandSim *)track)->getGeantPID());
// static Int_t good_p = 0, bad_p = 0, good_pim = 0, bad_pim = 0;

// if ( ((KParticleCandSim *)track)->getGeantParentPID() <= 0 ) return;
// if ( ((KParticleCandSim *)track)->getGeantParentPID() > 0 ) return;
// if (charge < 0 and ((KParticleCandSim *)track)->getGeantPID() == 14) ++bad_p;
// if (charge > 0 and ((KParticleCandSim *)track)->getGeantPID() == 14) ++good_p;
// if (charge < 0 and ((KParticleCandSim *)track)->getGeantPID() == 9) ++good_pim;
// if (charge > 0 and ((KParticleCandSim *)track)->getGeantPID() == 9) ++bad_pim;
// printf("good_p=%d   bad_p=%d   good_pim=%d   bad_pim=%d\n", good_p, bad_p, good_pim, bad_pim);
//     Float_t RichPhi   = track->getRichPhi();
//     Float_t RichTheta = track->getRichTheta();
//     Float_t Phi       = track->getPhi();
//     Float_t Theta     = track->getTheta();
//     Float_t dphi      = fabs(RichPhi-Phi);
//     Float_t dtheta    = fabs(RichTheta-Theta);
//
//     if (dphi > 180.0)        dphi = 360.0-dphi;
//     if (dtheta > 180.0)        dtheta = 360.0-dtheta;

/*
    // PID for Leptons
    // sigma = 1.5 degree -> 3 sigma = 4.5 (4.5*4.5 = 20.25)
    if (RichPhi >= 0.0 && (dphi*dphi + dtheta*dtheta) < 20.25) {
        if(Polarity > 0) {
            trackPID[3] = PID_elp; // e+
        }
        if(Polarity < 0) {
            trackPID[3] = PID_elm; // e-
        }
    }
*/
}

bool KTifiniAnalysis::makePID_Beta(const KParticleCand *cand, KT::ParticleID pid)
{
    if (!cand)
        return false;

    Float_t beta = cand->getBeta();
    Float_t cut_val = cand->getMomentum()/sqrt( cand->getMomentum()*cand->getMomentum() +
    HPhysicsConstants::mass(pid)*HPhysicsConstants::mass(pid) );

    KCutInside<Float_t> beta_cut(cut_val-0.075, cut_val+0.075, KT::WEAK, KT::WEAK);

    if (beta_cut.test(beta))
        return true;
    else
        return false;
}

// void KTifiniAnalysis::ConfigureOptions(int & n, option * & long_options)
// {
//     n = 0;
//     long_options = nullptr;
// }
//
//
// int KTifiniAnalysis::Configure(int /*c*/, const char * /*optarg*/)
// {
//     return 0;
// }

void KTifiniAnalysis::Usage() const
{
}
