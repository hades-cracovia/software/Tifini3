#include <cppunit/extensions/HelperMacros.h>

#include <TApplication.h>

#include "KTrack.h"
#include "KTools.h"

#if defined(HYDRA1COMP) && !defined(HYDRA1COMPBT)
#include "KTools.h"
#else
#include "hparticletool.h"
#endif

#include "hmdcsizescells.h"

class TracksCase : public CPPUNIT_NS::TestFixture
{
    CPPUNIT_TEST_SUITE( TracksCase );
    CPPUNIT_TEST( HadesParamTests );
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp();
    void tearDown();

protected:
    void HadesParamTests();
};

CPPUNIT_TEST_SUITE_REGISTRATION( TracksCase );

#define PR(x) std::cout << "++DEBUG: " << #x << " = |" << x << "| (" << __FILE__ << ", " << __LINE__ << ")\n";

void TracksCase::setUp()
{
}

void TracksCase::tearDown()
{
}

void correct(TVector3 & b, const TVector3 & d) {
    if (d.Z()) {
        b.SetX(b.X()-d.X()*b.Z()/fabs(d.Z()));
        b.SetY(b.Y()-d.Y()*b.Z()/fabs(d.Z()));
        b.SetZ(0);
    }
}

void correct(HGeomVector & b, const HGeomVector & d) {
    if (b.Z()) {
        b.setX(b.getX()-d.getX()*b.getZ()/fabs(d.getZ()));
        b.setY(b.getY()-d.getY()*b.getZ()/fabs(d.getZ()));
        b.setZ(0);
    }
}

void TracksCase::HadesParamTests()
{
    HGeomVector b, d;
    TVector3 base(0, 0, 0), vd;
    KTrack track;

    double pi2 = TMath::PiOver2();
    double pi4 = TMath::PiOver4();
    
    track.SetXYZT(1, 1, 0, 139);
    KTifini::setHadesTrackParams(base, track.Vect(), &track);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, track.getZ(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, track.getR(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(pi4, track.Phi(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(pi2, track.Theta(), 0.0001);

    HParticleTool::calcSegVector(track.getZ(), track.getR(), track.Phi(), track.Theta(), b, d);
    vd = track.Vect(); vd.SetMag(1);
    correct(base, vd);
    correct(b, d);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.X(), b.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Y(), b.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Z(), b.Z(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.X(), d.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Y(), d.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Z(), d.Z(), 0.0001);

    track.SetXYZT(1, 0, 1, 139);
    KTifini::setHadesTrackParams(base, track.Vect(), &track);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, track.getZ(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, track.getR(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, track.Phi(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(pi4, track.Theta(), 0.0001);

    HParticleTool::calcSegVector(track.getZ(), track.getR(), track.Phi(), track.Theta(), b, d);
    vd = track.Vect(); vd.SetMag(1);
    correct(base, vd);
    correct(b, d);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.X(), b.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Y(), b.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Z(), b.Z(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.X(), d.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Y(), d.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Z(), d.Z(), 0.0001);

    track.SetXYZT(0, 1, 1, 139);
    KTifini::setHadesTrackParams(base, track.Vect(), &track);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, track.getZ(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, track.getR(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(pi2, track.Phi(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(pi4, track.Theta(), 0.0001);

    HParticleTool::calcSegVector(track.getZ(), track.getR(), track.Phi(), track.Theta(), b, d);
    vd = track.Vect(); vd.SetMag(1);
    correct(base, vd);
    correct(b, d);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.X(), b.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Y(), b.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Z(), b.Z(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.X(), d.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Y(), d.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Z(), d.Z(), 0.0001);

    base.SetXYZ(1, 0, 0);
    track.SetXYZT(1, 0, 1, 139);
    KTifini::setHadesTrackParams(base, track.Vect(), &track);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(-1, track.getZ(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, track.getR(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, track.Phi(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(pi4, track.Theta(), 0.0001);

    HParticleTool::calcSegVector(track.getZ(), track.getR(), track.Phi(), track.Theta(), b, d);
    vd = track.Vect(); vd.SetMag(1);
    correct(base, vd);
    correct(b, d);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.X(), b.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Y(), b.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Z(), b.Z(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.X(), d.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Y(), d.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Z(), d.Z(), 0.0001);

    base.SetXYZ(0, 1, 0);
    track.SetXYZT(1, 0, 1, 139);
    KTifini::setHadesTrackParams(base, track.Vect(), &track);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, track.getZ(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(1, track.getR(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, track.Phi(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(pi4, track.Theta(), 0.0001);

    HParticleTool::calcSegVector(track.getZ(), track.getR(), track.Phi(), track.Theta(), b, d);
    vd = track.Vect(); vd.SetMag(1);
    correct(base, vd);
    correct(b, d);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.X(), b.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Y(), b.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Z(), b.Z(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.X(), d.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Y(), d.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Z(), d.Z(), 0.0001);

    base.SetXYZ(1, 0, 0);
    track.SetXYZT(0, 1, 1, 139);
    KTifini::setHadesTrackParams(base, track.Vect(), &track);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, track.getZ(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(-1, track.getR(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(pi2, track.Phi(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(pi4, track.Theta(), 0.0001);

    HParticleTool::calcSegVector(track.getZ(), track.getR(), track.Phi(), track.Theta(), b, d);
    vd = track.Vect(); vd.SetMag(1);
    correct(base, vd);
    correct(b, d);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.X(), b.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Y(), b.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Z(), b.Z(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.X(), d.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Y(), d.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Z(), d.Z(), 0.0001);

    base.SetXYZ(1, 0, 1);
    track.SetXYZT(0, 1, 1, 139);
    KTifini::setHadesTrackParams(base, track.Vect(), &track);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(1, track.getZ(), 0.0001);  
    CPPUNIT_ASSERT_DOUBLES_EQUAL(-1, track.getR(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(pi2, track.Phi(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(pi4, track.Theta(), 0.0001);

    HParticleTool::calcSegVector(track.getZ(), track.getR(), track.Phi(), track.Theta(), b, d);
    vd = track.Vect(); vd.SetMag(1);
    correct(base, vd);
    correct(b, d);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.X(), b.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Y(), b.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Z(), b.Z(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.X(), d.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Y(), d.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Z(), d.Z(), 0.0001);

    base.SetXYZ(1, 1, 1);
    track.SetXYZT(0, 1, 1, 139);
    KTifini::setHadesTrackParams(base, track.Vect(), &track);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(0, track.getZ(), 0.0001);  
    CPPUNIT_ASSERT_DOUBLES_EQUAL(-1, track.getR(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(pi2, track.Phi(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(pi4, track.Theta(), 0.0001);

    HParticleTool::calcSegVector(track.getZ(), track.getR(), track.Phi(), track.Theta(), b, d);
    vd = track.Vect(); vd.SetMag(1);
    correct(base, vd);
    correct(b, d);

    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.X(), b.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Y(), b.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(base.Z(), b.Z(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.X(), d.X(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Y(), d.Y(), 0.0001);
    CPPUNIT_ASSERT_DOUBLES_EQUAL(vd.Z(), d.Z(), 0.0001);
}
