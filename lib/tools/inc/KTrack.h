/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011 Rafał Lalik <rafal.lalik@ph.tum.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef KTRACK_H
#define KTRACK_H

#include "Tifini3Config.h"

#if defined(HYDRA1COMP) && !defined(HYDRA1COMPBT)
#include "KTools.h"
#else
#include "hparticletool.h"
#endif
class TTree;

class KTrack : public TLorentzVector
{
public:
    KTrack();
    virtual ~KTrack();
    void useAnglesDeg(bool use_deg);
    Bool_t isAnglesDeg() const { return flag_use_deg; }
    Float_t r2d() const { return fr2d; }

    KTrack & operator=(const KParticleCand & track);
    KTrack & operator=(const TLorentzVector & track);

    void    setR(Float_t r)                 { fR = r;                    }
    Float_t getR() const                    { return fR;                 }

    void    setZ(Float_t z)                 { fZ = z;                    }
    Float_t getZ() const                    { return fZ;                 }

    enum Branches {
        bE          = 1 << 0,
        bM          = 1 << 1,
        bP          = 1 << 2,
        bTheta      = 1 << 3,
        bCosTheta   = 1 << 4,
        bPhi        = 1 << 5,
        bY          = 1 << 6,
        bPt         = 1 << 7,
        bDSinfo     = 1 << 8,
        bPv         = 1 << 9,
    };
    bool setTree(TTree * tree, const TString& unique_name, UInt_t b = 0xff);
    void fill();

private:
    TTree * tree;
    TString unique_name;

    Int_t track_case;       // 0-TLorentzVector, 1-KParticleCand
    KParticleCand tr_ah;

    Bool_t flag_use_deg;
    Float_t fr2d;

    Float_t fE, fM, fP;
    Float_t fPx, fPy, fPz;
    Float_t fTheta, fCosTheta, fPhi;
    Float_t fY, fPt;
    Float_t fR, fZ;

    Short_t fSystem;
    Short_t fCharge;
    Short_t fTofRec;
    Float_t fBeta;
    Float_t fMDCdEdx;
    Float_t fTOFdEdx;

    ClassDef(KTrack,1);
};

#endif // KTRACK_H
