/*
    <one line to give the program's name and a brief idea of what it does.>
    Copyright (C) 2011 Rafał Lalik <rafal.lalik@ph.tum.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program. If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef KTRACKCALIBRATER_PP35_H
#define KTRACKCALIBRATER_PP35_H

#include "KTrackCalibrater.h"

#ifdef HYDRA1COMP
class KTrackCalibrater_pp35 : public KTrackCalibrater
{
public:
    KTrackCalibrater_pp35() : KTrackCalibrater() {}

    void calibrate(KParticleCand * cand);
};
#endif

#endif // KTRACKCALIBRATER_PP35_H
