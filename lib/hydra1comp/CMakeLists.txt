#----------------------------------------------------------------------------

GENERATE_LIBRARY(
    TARGET Tifini3H1Comp
    NAMESPACE Tifini
    SOURCES
        src/KParticleCand.cxx
        src/KParticleCandSim.cxx

    HEADERS
        inc/KParticleCand.h
        inc/KParticleCandSim.h

    LIBRARIES
        # ROOT
        Core
        Physics
        # HYDRA

    INCLUDE_DIRS
        inc
        ${HYDRA_INCLUDE_DIR}
        ${HYDRA2_INCLUDE_DIR}

    PUBLIC_INCLUDE_DIRS
        ${ROOT_INCLUDE_DIRS}
)
