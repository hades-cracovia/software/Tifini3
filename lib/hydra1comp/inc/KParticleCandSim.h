#ifndef KPARTICLECANDSIM_H
#define KPARTICLECANDSIM_H

#include <TMath.h>
#include <TObject.h>
#include <TClonesArray.h>
#include <TLorentzVector.h>

#include "KParticleCand.h"

// Modified by A. Schmah 07.04.2009: Sim info added
// Modified by A. Schmah 17.04.2009: Forward wall info added
// Modified by A. Schmah 18.06.2009: New structure with Sim track and WallHit
// Modified by R. Lalik  04.04.2013: Ancestor chain implemented

#include "hpidtrackcandsim.h"

class KParticleCandSim : public KParticleCand
{
private:

    static const Int_t AncestorLevel = 4;

    // sim info
    Int_t   fGeantPID;              // Geant PID value
    Int_t   fGeantParentPID;        // Geant PID value of the Parent
    Int_t   fGeantGrandParentPID;   // Grand Parent PID
//    Int_t   fGeantAncestorPID[AncestorLevel+1];               // Geant PID value of the GrandGrantparent
    UInt_t  fGeantCorrTrackIds;     // Geant number of correlated track ids
    Float_t fGeantxMom;
    Float_t fGeantyMom;
    Float_t fGeantzMom;
    Float_t fGeantxVertex;
    Float_t fGeantyVertex;
    Float_t fGeantzVertex;
    Int_t   fGeantParentTrackNum; // parent track number
    Int_t   fGeantCreationMechanism;
    Int_t   fGeantMediumNumber;
    Int_t   fGenInfo, fGenInfo1, fGenInfo2;
    Float_t fGenWeight;
    Bool_t  fisEmbSimTrack;
    Bool_t  fisHiddenPion;

public:
    KParticleCandSim();
    KParticleCandSim(HPidTrackCandSim & cand);

    virtual ~KParticleCandSim();

    void    setGeantPID(Int_t a)                { fGeantPID = a;     }
    Int_t   getGeantPID() const                 { return fGeantPID;  }

    void    setGeantParentPID(Int_t a)          { fGeantParentPID = a;     }
    Int_t   getGeantParentPID() const           { return fGeantParentPID;  }

    void    setGeantGrandParentPID(Int_t a)     { fGeantGrandParentPID = a;    }
    Int_t   getGeantGrandParentPID() const      { return fGeantGrandParentPID; }

    void    setGeantCorrTrackIds(UInt_t a)      { fGeantCorrTrackIds = a;       }
    UInt_t  getGeantCorrTrackIds() const        { return fGeantCorrTrackIds;    }

    void    setGeantxMom(Float_t a)             { fGeantxMom = a;               }
    Float_t getGeantxMom() const                { return fGeantxMom;            }

    void    setGeantyMom(Float_t a)             { fGeantyMom = a;               }
    Float_t getGeantyMom() const                { return fGeantyMom;            }

    void    setGeantzMom(Float_t a)             { fGeantzMom = a;               }
    Float_t getGeantzMom() const                { return fGeantzMom;            }

    void    setGeantxVertex(Float_t a)          { fGeantxVertex = a;            }
    Float_t getGeantxVertex() const             { return fGeantxVertex;         }

    void    setGeantyVertex(Float_t a)          { fGeantyVertex = a;            }
    Float_t getGeantyVertex() const             { return fGeantyVertex;         }

    void    setGeantzVertex(Float_t a)          { fGeantzVertex = a;            }
    Float_t getGeantzVertex() const             { return fGeantzVertex;         }

    void    setGeantParentTrackNum(Int_t a)     { fGeantParentTrackNum = a;     }
    Int_t   getGeantParentTrackNum() const      { return fGeantParentTrackNum;  }

    void    setGeantCreationMechanism(Int_t a)  { fGeantCreationMechanism = a;      }
    Int_t   getGeantCreationMechanism() const   { return fGeantCreationMechanism;   }

    void    setGeantMediumNumber(Int_t a)       { fGeantMediumNumber = a;       }
    Int_t   getGeantMediumNumber() const        { return fGeantMediumNumber;    }

    void    setGenInfo(Int_t i)                 { fGenInfo = i;                 }
    Int_t   getGenInfo() const                  { return fGenInfo;              }

    void    setGenInfo1(Int_t i)                { fGenInfo1 = i;                }
    Int_t   getGenInfo1() const                 { return fGenInfo1;             }

    void    setGenInfo2(Int_t i)                { fGenInfo2 = i;                }
    Int_t   getGenInfo2() const                 { return fGenInfo2;             }

    void    setGenWeight(Float_t w)             { fGenWeight = w;               }
    Float_t getGenWeight() const                { return fGenWeight;            }

    void    setisEmbSimTrack(Bool_t a)          { fisEmbSimTrack = a;           }
    Bool_t  getisEmbSimTrack() const            { return fisEmbSimTrack;        }

    void    setisHiddenPion(Bool_t a)           { fisHiddenPion = a;            }
    Bool_t  getisHiddenPion() const             { return fisHiddenPion;         }

    HPidTrackCandSim * getTrackCand() const     { return tcand; }

    virtual void  print(UInt_t selection = 0b111);

private:
    HPidTrackCandSim * tcand;

    ClassDef(KParticleCandSim,1)  // A simple Sim track of a particle
};

#endif // KPARTICLECANDSIM_H
