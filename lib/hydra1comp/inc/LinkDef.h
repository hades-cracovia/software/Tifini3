#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

//#pragma link C++ class KTools+;
#pragma link C++ class KParticleCand+;
#pragma link C++ class KParticleCandSim+;

#endif
