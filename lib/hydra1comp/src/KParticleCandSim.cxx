#include "KParticleCandSim.h"

#include "hcategory.h"
#include "hgeantkine.h"
#include "hpidgeanttrackset.h"

KParticleCandSim::KParticleCandSim()
    : KParticleCand()
    , fGeantCorrTrackIds(0)
    , fGeantxMom(-1), fGeantyMom(-1), fGeantzMom(-1), fGeantxVertex(-1), fGeantyVertex(-1), fGeantzVertex(-1)
    , fGeantParentTrackNum(0), fGeantCreationMechanism(0), fGeantMediumNumber(0)
    , fGenInfo(0), fGenInfo1(0), fGenInfo2(0), fGenWeight(0)
    , fisEmbSimTrack(0), fisHiddenPion(0)
{
}

KParticleCandSim::KParticleCandSim(HPidTrackCandSim& cand) : KParticleCand(cand)
{
    tcand = &cand;
    HPidGeantTrackSet * ptrackset = cand.getGeantTrackSet();

    setGeantPID(ptrackset->getGeantPID());
    setGeantParentPID(ptrackset->getGeantParentID());
    setGeantGrandParentPID(ptrackset->getGeantGrandParentID());

    setGeantCorrTrackIds(ptrackset->getNCorrelatedTrackIds());
    setGeantxMom(ptrackset->getGeantMomX(0));
    setGeantyMom(ptrackset->getGeantMomY(0));
    setGeantzMom(ptrackset->getGeantMomZ(0));
    setGeantxVertex(ptrackset->getGeantVertexX(0));
    setGeantyVertex(ptrackset->getGeantVertexY(0));
    setGeantzVertex(ptrackset->getGeantVertexZ(0));
    setGeantParentTrackNum(ptrackset->getGeantParentTrackID(0));
    setGeantMediumNumber(ptrackset->getGeantMediumID(0));
    setGeantCreationMechanism(ptrackset->getGeantProcessID(0));
    setGenInfo(ptrackset->getGenInfo());
    setGenInfo1(ptrackset->getGenInfo1());
    setGenInfo2(ptrackset->getGenInfo2());
    setGenWeight(ptrackset->getGenWeight());
}

KParticleCandSim::~KParticleCandSim()
{
}

void KParticleCandSim::print(UInt_t selection)
{
    if (selection >> 0 & 0x01)
        tcand->print();
    if (selection >> 1 & 0x01)
        getTrackCand()->getGeantTrackSet()->print();
    if (selection >> 2 & 0x01)
    {
        std::cout
            << "GEANT INFO" << std::endl
            << " Momentum: " << getTrackCand()->getGeantTrackSet()->getTotalMomentum() << " => "
            << getGeantxMom() << ", " << getGeantyMom() << ", " << getGeantzMom() << std::endl
            << " Parent track num: " << getGeantParentTrackNum()
            << std::endl;
    }
}

ClassImp(KParticleCandSim);
